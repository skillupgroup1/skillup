<?php
// note that database name should be used as an index for $db array
/**
 *
 */
class Database
{

  function __construct($argument='') {
    $this->db = array(
      'hostname' => 'localhost',
      'username' => 'root',
      'password' => '123456',
      'database' => 'news_forum',
      'dbdriver' => 'mysqli',
      'dbprefix' => '',
      'pconnect' => TRUE,
      'db_debug' => TRUE,
      'cache_on' => FALSE,
      'cachedir' => '',
      'char_set' => 'utf8',
      'dbcollat' => 'utf8_general_ci'
    );
  }
}
