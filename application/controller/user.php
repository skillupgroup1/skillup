<?php
/**
 * render method inside Object will render the view for any method we will make inside controller
 */
class User extends Main_Controller
{

  function __construct($argument='')
  {
    parent::__construct();
    $this->load_model= User::Load_model('user_model');  //this is how to use methodes inside model you created inside model folders
    ///note that you should use a calss with same name of model file and class should have only first chrachter as upper case 
    // echo "<pre>";print_r($this->Load_model('user_model'));die;
    echo "this message from user controller in his constructor<br>";
  }

  function index()   //we should use it for after login page
  {
    $this->render('user_view');
    echo " I am index method inside user controller";
  }

  function test ($value='')
  {
    echo "this is method test, this message from  user controller and argument passed is $value";

  }

  function loading($data='')
  {
    $this->render('application/views/test/test',$data);
  }

  function LoadDataFromModel()
  {
    // echo "<pre>"; print_r($this->load_model('user_model')); die;
    // echo "<pre>";print_r($this->load_model->userMethod());
    $this->load_model->userMethod();
  }

}
