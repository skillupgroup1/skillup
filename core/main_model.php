<?php
/**
 *
 */
require_once 'core/orm.php';

class Main_Model
{

  function __construct($argument='')
  {
    // echo "this is main model this message from main model construtor <br>";
    $this->Orm_obj = new Orm();
    // echo "<pre>"; var_dump($this->Orm_obj->conn->connect_error === NULL); die;
    if ($this->Orm_obj->conn->connect_error !== NULL) {
      echo "There is connection problem to database<br>";
      return false;
    }

  }
     function insert($data,$table,$where='') // $data = array ('column' => 5)
    {
      $arr = $this->Orm_obj->insert($data,$table,$where='');
      return $arr;
    }

    function delete ($table,$where)      //any argument should be string
    {
      $arr = $this->Orm_obj->delete($table,$where);
      return $arr;
    }

    function update($data,$table,$where)  // eg. $data = array ('column' => 5)
    {
      $arr = $this->Orm_obj->update($data,$table,$where);
      return $arr;
    }

    function select($data='',$table,$where='') // eg. $data = array ('column') and if $data is empty string it will be considered as *(astrisk)
    {
     $arr = $this->Orm_obj->select($data,$table,$where);
     return $arr;
    }

    function myQuery($myString='') {    //$string is a string query
      $arr = $this->Orm_obj->myQuery($myString);
      return $arr;
    }




}
