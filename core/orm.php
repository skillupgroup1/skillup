<?php
/**
 *
 */
require_once 'application/config/database.php';
require_once 'application/config/config.php';

class Orm {

  function __construct($argument='') {
      $db = new Database();
      $configuration = new Config();
      $servername = $db->db['hostname'];
      $username = $db->db['username'];
      $password = $db->db['password'];
      $database = $db->db['database'];
      $conn = new mysqli($servername, $username, $password, $database);
      $this->conn = $conn;
      $this->view = $configuration->default_page;

 // Check connection
      if ($conn->connect_error) {
           die("Connection failed: " . $conn->connect_error);
      }
      // echo "Connected successfully This message from orm<br>";
  }

//********database functions to make queries*******//

/*  queries return a resultset */
// if ($result = $conn->query($query)) {
// mysqli_query($link, "SELECT Name FROM City LIMIT 10")
//     $result->close();
// }
// $data should be normal array of strings or only one string variable
//any argument should be string   // eg. $data = array ('column' => 5)
     function insert($data,$table,$where='') {
          $cols=implode(",",array_keys($data));
          $values =implode(",",$data);
          // echo "<pre>";var_dump($values); die;
          if ($where !== '') {
            $query="INSERT INTO $table ($cols) VALUES ($values) WHERE $where";
            $result = $this->conn->query($query);
            mysqli_free_result($result);
            return false;
          }
          $query= "INSERT INTO $table ($cols) VALUES ($values)";
          $result = $this->conn->query($query);
          mysqli_free_result($result);
          return true;
     }

     //any argument should be string
      function delete($table,$where) {
          $query="DELETE FROM $table WHERE $where";
          $result = $this->conn->query($query);
          mysqli_free_result($result);
          return true;
     }

     // $data should be normal array of strings or only one string variable
     //any argument should be string // eg. $data = array ('column' => 5)
      function update($data,$table,$where) {
         $field = '';
         foreach($data as $col=>$value)
         {
             $field .="$col= $value,";
         }
         $field = rtrim($field,',');
      $query="UPDATE $table SET $field WHERE $where";
      $result = $this->conn->query($query);
      mysqli_free_result($result);
      return true;
     }

    // $data should be normal array of strings or only one string variable
    //any argument should be string // eg. $data = array ('column')
      function select($data='',$table,$where = '') {
       $field = '';
       $NOfArg = func_num_args();
           if ($where !== '') {
                 if ($data === '') {
                   $col = '*';
                   $query="SELECT $col FROM $table WHERE $where";
                   $result = $this->conn->query($query);
                   $arr = $result->fetch_all();
                   mysqli_free_result($result);
                   return $arr;
                 }
                 foreach($data as $value) {
                   $field .="'$value',";
                 }
                 $col = rtrim($field,",");
                 $query="SELECT $col FROM $table WHERE $where";
                 $result = $this->conn->query($query);
                 $arr = $result->fetch_all();
                 mysqli_free_result($result);
                 return $arr;
           }
           if ($data === '') {
               $col = '*';
               $query="SELECT $col FROM $table";
               $result = $this->conn->query($query);
               //  var_dump(get_class_methods($result));
               //  var_dump($result->fetch_all());
               $arr = $result->fetch_all();
               mysqli_free_result($result);
               return $arr;
           }
           foreach($data as $value) {
             $field .="'$value',";
           }
           $col = rtrim($field,",");
           $query="SELECT $col FROM $table";
           $result = $this->conn->query($query);
           $arr = $result->fetch_all();
           mysqli_free_result($result);
           return $arr;
    }

    //this function is to use direct query
      function myQuery($myString='') {
       $arr = $this->conn->query($myString)->fetch_all();
      //  print_r($this->conn->query($myString)->fetch_all());
       return $arr;
    }




}
